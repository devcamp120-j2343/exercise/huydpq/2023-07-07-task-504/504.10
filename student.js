import { Person } from "./person.js";

class Student extends Person {
    constructor(personName, personAge, gender, standark, collegeName, grade){
        super(personName,personAge,gender)
        this.standark = standark;
        this.collegeName = collegeName;
        this.grade = grade;

    }

    getStudentInfo(){
        return this.standark;
    };
    getGrade (){
        return this.grade
    };
    setGrade(paramGrade){
        this.grade = paramGrade;
    }

}
export {Student}